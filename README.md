# Using zenodo repository with Argos

The repository-deposit-zenodo module implements the [https://code-repo.d4science.org/MaDgiK-CITE/repository-deposit-base](https://) interface for the zenodo repository.

## Setup

After creating the jar from the project, environment variables should be set since they are used in the application.properties
1) STORAGE_TMP_ZENODO - a temporary storage needed
2) CONFIGURATION_ZENODO - path to json file which includes the configuration for the repository

### JSON configuration file

The following fields should be set:<br>
**depositType** - an integer representing how the dmp user can deposit in the repository,<br>
a. **0** stands for system deposition meaning the dmp is deposited using argos credentials to the repository,<br>
b. **1** stands for user deposition in which the argos user specifies his/her own credentials to the repository,<br>
c. **2** stands for both ways deposition if the repository allows the deposits of dmps to be made from both argos and users accounts<br>
**repositoryId** - unique identifier for the repository<br>
**accessToken** - access token provided for the system type deposits<br>
**repositoryUrl** - repository's api url e.g "https://sandbox.zenodo.org/api/" <br>
**repositoryAuthorizationUrl** - repository's authorization url e.g. "https://sandbox.zenodo.org/oauth/authorize" <br>
**repositoryRecordUrl** - repository's record url, this url is used to index dmps that are created e.g. "https://sandbox.zenodo.org/record/" <br>
**repositoryAccessTokenUrl** - repository's access token url e.g. "https://sandbox.zenodo.org/oauth/token" <br>
**repositoryClientId** - repository's client id<br>
**repositoryClientSecret** - repository's client secret<br>
**redirectUri** - redirect uri to argos after the oauth2 flow from the repository<br>
**hasLogo** - if the repository has a logo<br>