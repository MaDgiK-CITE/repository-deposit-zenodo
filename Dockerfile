####################################### Build stage #######################################
FROM maven:3.6.3-openjdk-11-slim AS build-stage

COPY . /build/
WORKDIR /build/
RUN mvn clean package -DskipTests

ARG CREPO_BINARIES_REPO_URL
ARG CREPO_BINARIES_CREDENTIAL
ARG BUILD_VERSION
ENV CREPO_BINARIES_REPO_URL=$CREPO_BINARIES_REPO_URL
ENV CREPO_BINARIES_CREDENTIAL=$CREPO_BINARIES_CREDENTIAL
ENV BUILD_VERSION=$BUILD_VERSION

RUN curl --location --request PUT "${CREPO_BINARIES_REPO_URL}opendmp/repository-jars/zenodo/zenodo-deposit-${BUILD_VERSION}.jar" \
--header "Authorization: Basic ${CREPO_BINARIES_CREDENTIAL}" \
--header "Content-Type: application/json" \
--data-binary "@/build/target/repositorydepositzenodo-1.0.0-SNAPSHOT.jar"