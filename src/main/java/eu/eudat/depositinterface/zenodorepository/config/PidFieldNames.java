package eu.eudat.depositinterface.zenodorepository.config;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PidFieldNames {
    @JsonProperty("pidName")
    private String pidName;
    @JsonProperty("pidTypeName")
    private String pidTypeName;

    public PidFieldNames() {}

    public PidFieldNames(String pidName, String pidTypeName) {
        this.pidName = pidName;
        this.pidTypeName = pidTypeName;
    }

    public String getPidName() {
        return pidName;
    }
    public void setPidName(String pidName) {
        this.pidName = pidName;
    }

    public String getPidTypeName() {
        return pidTypeName;
    }
    public void setPidTypeName(String pidTypeName) {
        this.pidTypeName = pidTypeName;
    }
}
