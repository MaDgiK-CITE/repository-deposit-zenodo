package eu.eudat.depositinterface.zenodorepository.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.eudat.depositinterface.repository.RepositoryDepositConfiguration;

public class ZenodoConfig {

    private enum DepositType {
        SystemDeposit(0), UserDeposit(1), BothWaysDeposit(2);

        private final int value;

        DepositType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static DepositType fromInteger(int value) {
            switch (value) {
                case 0:
                    return SystemDeposit;
                case 1:
                    return UserDeposit;
                case 2:
                    return BothWaysDeposit;
                default:
                    throw new RuntimeException("Unsupported Deposit Type");
            }
        }
    }

    @JsonProperty("depositType")
    private int depositType;
    @JsonProperty("repositoryId")
    private String repositoryId;
    @JsonProperty("accessToken")
    private String accessToken;
    @JsonProperty("repositoryUrl")
    private String repositoryUrl;
    @JsonProperty("repositoryAuthorizationUrl")
    private String repositoryAuthorizationUrl;
    @JsonProperty("repositoryRecordUrl")
    private String repositoryRecordUrl;
    @JsonProperty("repositoryAccessTokenUrl")
    private String repositoryAccessTokenUrl;
    @JsonProperty("repositoryClientId")
    private String repositoryClientId;
    @JsonProperty("repositoryClientSecret")
    private String repositoryClientSecret;
    @JsonProperty("redirectUri")
    private String redirectUri;
    @JsonProperty("hasLogo")
    private boolean hasLogo;
    @JsonProperty("logo")
    private String logo;
    @JsonProperty("doiFunder")
    private String doiFunder;
    @JsonProperty("community")
    private String community;
    @JsonProperty("affiliation")
    private String affiliation;
    @JsonProperty("domain")
    private String domain;

    public int getDepositType() {
        return depositType;
    }
    public void setDepositType(int depositType) {
        this.depositType = depositType;
    }

    public String getRepositoryId() {
        return repositoryId;
    }
    public void setRepositoryId(String repositoryId) {
        this.repositoryId = repositoryId;
    }

    public String getAccessToken() {
        return accessToken;
    }
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRepositoryUrl() {
        return repositoryUrl;
    }
    public void setRepositoryUrl(String repositoryUrl) {
        this.repositoryUrl = repositoryUrl;
    }

    public String getRepositoryAuthorizationUrl() {
        return repositoryAuthorizationUrl;
    }
    public void setRepositoryAuthorizationUrl(String repositoryAuthorizationUrl) {
        this.repositoryAuthorizationUrl = repositoryAuthorizationUrl;
    }

    public String getRepositoryRecordUrl() {
        return repositoryRecordUrl;
    }
    public void setRepositoryRecordUrl(String repositoryRecordUrl) {
        this.repositoryRecordUrl = repositoryRecordUrl;
    }

    public String getRepositoryAccessTokenUrl() {
        return repositoryAccessTokenUrl;
    }
    public void setRepositoryAccessTokenUrl(String repositoryAccessTokenUrl) {
        this.repositoryAccessTokenUrl = repositoryAccessTokenUrl;
    }

    public String getRepositoryClientId() {
        return repositoryClientId;
    }
    public void setRepositoryClientId(String repositoryClientId) {
        this.repositoryClientId = repositoryClientId;
    }

    public String getRepositoryClientSecret() {
        return repositoryClientSecret;
    }
    public void setRepositoryClientSecret(String repositoryClientSecret) {
        this.repositoryClientSecret = repositoryClientSecret;
    }

    public String getRedirectUri() {
        return redirectUri;
    }
    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    public boolean isHasLogo() {
        return hasLogo;
    }
    public void setHasLogo(boolean hasLogo) {
        this.hasLogo = hasLogo;
    }

    public String getLogo() {
        return logo;
    }
    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getDoiFunder() {
        return doiFunder;
    }
    public void setDoiFunder(String doiFunder) {
        this.doiFunder = doiFunder;
    }

    public String getCommunity() {
        return community;
    }
    public void setCommunity(String community) {
        this.community = community;
    }

    public String getAffiliation() {
        return affiliation;
    }
    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    public String getDomain() {
        return domain;
    }
    public void setDomain(String domain) {
        this.domain = domain;
    }

    public RepositoryDepositConfiguration toRepoConfig() {
        RepositoryDepositConfiguration config = new RepositoryDepositConfiguration();
        config.setDepositType(this.depositType);
        config.setRepositoryId(this.repositoryId);
        config.setAccessToken(this.accessToken);
        config.setRepositoryUrl(this.repositoryUrl);
        config.setRepositoryAuthorizationUrl(this.repositoryAuthorizationUrl);
        config.setRepositoryRecordUrl(this.repositoryRecordUrl);
        config.setRepositoryAccessTokenUrl(this.repositoryAccessTokenUrl);
        config.setRepositoryClientId(this.repositoryClientId);
        config.setRepositoryClientSecret(this.repositoryClientSecret);
        config.setRedirectUri(this.redirectUri);
        config.setHasLogo(this.hasLogo);
        return config;
    }
}
