package eu.eudat.depositinterface.zenodorepository.config;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service("zenodoConfigLoader")
public class ConfigLoaderImpl implements ConfigLoader{
    private static final Logger logger = LoggerFactory.getLogger(ConfigLoaderImpl.class);
    private static final ObjectMapper mapper = new ObjectMapper();

    private List<String> relatedIdentifiers = new ArrayList<>();
    private List<String> acceptedPidTypes = new ArrayList<>();
    private PidFieldNames pidFieldNames = new PidFieldNames();
    private List<ZenodoConfig> zenodoConfig = new ArrayList<>();

    private final Environment environment;

    @Autowired
    public ConfigLoaderImpl(Environment environment){
        this.environment = environment;
    }

    @Override
    public List<String> getRelatedIdentifiers() {
        if (relatedIdentifiers == null || relatedIdentifiers.isEmpty()) {
            BufferedReader ids = new BufferedReader(new InputStreamReader(getStreamFromPath("relatedIdentifiers.txt")));
            relatedIdentifiers = ids.lines().collect(Collectors.toList());
        }
        return relatedIdentifiers;
    }

    @Override
    public List<String> getAcceptedPidTypes() {
        if (acceptedPidTypes == null || acceptedPidTypes.isEmpty()) {
            BufferedReader ids = new BufferedReader(new InputStreamReader(getStreamFromPath("acceptedPidTypes.txt")));
            acceptedPidTypes = ids.lines().collect(Collectors.toList());
        }
        return acceptedPidTypes;
    }

    @Override
    public PidFieldNames getPidFieldNames() {
        try {
            pidFieldNames = mapper.readValue(getStreamFromPath("datasetFieldsPid.json"), PidFieldNames.class);
        }
        catch (IOException e){
            logger.error(e.getLocalizedMessage(), e);
        }
        return pidFieldNames;
    }

    @Override
    public List<ZenodoConfig> getZenodoConfig() {
        if (zenodoConfig == null || zenodoConfig.isEmpty()) {
            try {
                zenodoConfig = mapper.readValue(getStreamFromPath(environment.getProperty("zenodo_plugin.configuration.zenodo")), new TypeReference<List<ZenodoConfig>>() {});
            } catch (IOException e) {
                logger.error(e.getLocalizedMessage(), e);
                return null;
            }
        }
        return zenodoConfig;
    }

    @Override
    public byte[] getLogo(String repositoryId) {
        if (!zenodoConfig.isEmpty()) {
            ZenodoConfig zenodoConfig = getZenodoConfig().stream().filter(x -> x.getRepositoryId().equals(repositoryId)).findFirst().orElse(null);
            if (zenodoConfig != null) {
                String logo = zenodoConfig.getLogo();
                InputStream logoStream;
                if (logo != null && !logo.isEmpty()) {
                    logoStream = getStreamFromPath(logo);
                }
                else {
                    logoStream = getClass().getClassLoader().getResourceAsStream("zenodo.jpg");
                }
                try {
                    return (logoStream != null) ? logoStream.readAllBytes() : null;
                }
                catch (IOException e) {
                    logger.error(e.getMessage(), e);
                }
            }
            return null;
        }
        return null;
    }

    @Override
    public InputStream getStreamFromPath(String filePath) {
        try {
            return new FileInputStream(filePath);
        } catch (FileNotFoundException e) {
            logger.info("loading from classpath");
            return getClass().getClassLoader().getResourceAsStream(filePath);
        }
    }
}
