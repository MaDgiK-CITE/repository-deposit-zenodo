package eu.eudat.depositinterface.zenodorepository.config;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DOIFunder {

    @JsonProperty("Funder")
    private String funder;
    @JsonProperty("DOI")
    private String DOI;

    public String getFunder() {
        return funder;
    }

    public void setFunder(String funder) {
        this.funder = funder;
    }

    public String getDOI() {
        return DOI;
    }

    public void setDOI(String DOI) {
        this.DOI = DOI;
    }
}
