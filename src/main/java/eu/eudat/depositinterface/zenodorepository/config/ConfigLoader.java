package eu.eudat.depositinterface.zenodorepository.config;

import java.io.InputStream;
import java.util.List;

public interface ConfigLoader {
    InputStream getStreamFromPath(String filePath);
    List<String> getRelatedIdentifiers();
    List<String> getAcceptedPidTypes();
    PidFieldNames getPidFieldNames();
    byte[] getLogo(String repositoryId);
    List<ZenodoConfig> getZenodoConfig();
}
