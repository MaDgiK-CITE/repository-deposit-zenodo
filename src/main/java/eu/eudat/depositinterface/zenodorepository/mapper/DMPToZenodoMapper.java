package eu.eudat.depositinterface.zenodorepository.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.eudat.depositinterface.enums.FieldType;
import eu.eudat.depositinterface.models.*;
import eu.eudat.depositinterface.zenodorepository.config.ConfigLoader;
import eu.eudat.depositinterface.zenodorepository.config.DOIFunder;
import eu.eudat.depositinterface.zenodorepository.config.PidFieldNames;
import eu.eudat.depositinterface.zenodorepository.config.ZenodoConfig;
import eu.eudat.depositinterface.zenodorepository.models.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class DMPToZenodoMapper {

    private static final Logger logger = LoggerFactory.getLogger(DMPToZenodoMapper.class);
    private static final ObjectMapper objectMapper = new ObjectMapper();

    private static ConfigLoader configLoader;
    private static PidFieldNames pidFieldNames;

    @Autowired
    public DMPToZenodoMapper(ConfigLoader configL){
        configLoader = configL;
        pidFieldNames = configLoader.getPidFieldNames();
    }

    private static List<DatasetFieldsDepositModel> findSchemanticValues(String relatedId, List<DatasetFieldsDepositModel> fields){
        return fields.stream().filter(f -> f.getSchematics().contains(relatedId)).collect(Collectors.toList());
    }

    private static Set<String> extractSchemanticValues(List<DatasetFieldsDepositModel> fields, List<String> acceptedPidTypes) throws JsonProcessingException{
        Set<String> values = new HashSet<>();
        for(DatasetFieldsDepositModel field: fields){
            String value = (String) field.getValue();
            if(value != null && !value.isEmpty()) {
                switch (FieldType.fromName(field.getRenderStyleType())) {
                    case FREE_TEXT:
                    case TEXT_AREA:
                    case RICH_TEXT_AREA:
                    case RADIO_BOX:
                    case DATE_PICKER:
                        values.add(value);
                        break;
                    case COMBO_BOX:
                        if (field.isMultiple()) {
                            List<String> selected = objectMapper.readValue(value, new TypeReference<List<String>>() {});
                            values.addAll(selected);
                        }
                        else {
                            values.add(value);
                        }
                        break;
                    case REGISTRIES:
                    case SERVICES:
                    case EXTERNAL_DATASETS:
                    case DATA_REPOSITORIES:
                    case PUB_REPOSITORIES:
                    case JOURNAL_REPOSITORIES:
                    case TAXONOMIES:
                    case PUBLICATIONS:
                        if (field.isMultiple()) {
                            List<String> selected = objectMapper.readValue(value, new TypeReference<List<String>>() {});
                            for (String s : selected) {
                                Map<String, String> valueMap = objectMapper.readValue(s, new TypeReference<Map<String, String>>() {});
                                String pid = valueMap.get(pidFieldNames.getPidName());
                                String pidType = valueMap.get(pidFieldNames.getPidTypeName());
                                if (acceptedPidTypes.contains(pidType)) {
                                    values.add(pid);
                                }
                            }
                        }
                        else {
                            Map<String, String> valueMap = objectMapper.readValue(value, new TypeReference<Map<String, String>>() {});
                            String pid = valueMap.get(pidFieldNames.getPidName());
                            String pidType = valueMap.get(pidFieldNames.getPidTypeName());
                            if (acceptedPidTypes.contains(pidType)) {
                                values.add(pid);
                            }
                        }
                        break;
                    case ORGANIZATIONS:
                    case RESEARCHERS:
                        if (field.isMultiple()) {
                            List<String> selected = objectMapper.readValue(value, new TypeReference<List<String>>() {});
                            for (String s : selected) {
                                Map<String, String> valueMap = objectMapper.readValue(s, new TypeReference<Map<String, String>>() {});
                                String pid = valueMap.get("reference");
                                if(pid != null) {
                                    values.add(pid);
                                }
                            }
                        }
                        else {
                            Map<String, String> valueMap = objectMapper.readValue(value, new TypeReference<Map<String, String>>() {});
                            String pid = valueMap.get("reference");
                            if(pid != null) {
                                values.add(pid);
                            }
                        }
                        break;
                    case DATASET_IDENTIFIER:
                        Map<String, String> valueMap = objectMapper.readValue(value, new TypeReference<Map<String, String>>() {});
                        values.add(valueMap.get("identifier"));
                        break;
                }
            }
        }
        return values;
    }

    public static ZenodoDeposit fromDMP(DMPDepositModel dmp, ZenodoConfig zenodoConfig) throws JsonProcessingException {
        Map<String, Object> extraProperties = dmp.getExtraProperties() != null ? new org.json.JSONObject(dmp.getExtraProperties()).toMap() : new HashMap<>();
        ZenodoDeposit deposit = new ZenodoDeposit();

        Map<String, Object> schematicsMap = new HashMap<>();

        List<ZenodoRelator> relatedIdentifiers = new ArrayList<>();
        List<ZenodoComunity> communities = new ArrayList<>();
        List<ZenodoContributor> contributors = new ArrayList<>();
        List<ZenodoCreator> creators = new ArrayList<>();
        List<ZenodoGrant> grants = new ArrayList<>();
        List<String> keywords = new ArrayList<>();
        List<String> references = new ArrayList<>();

        List<String> acceptedPidTypes = configLoader.getAcceptedPidTypes();

        for(DatasetDepositModel dataset: dmp.getDatasets()){

            for(String relatedId: configLoader.getRelatedIdentifiers()){

                List<DatasetFieldsDepositModel> fields = findSchemanticValues(relatedId, dataset.getFields());
                Set<String> values = extractSchemanticValues(fields, acceptedPidTypes);
                for(String value: values){
                    ZenodoRelator relator = new ZenodoRelator();
                    relator.setRelation(relatedId.substring(relatedId.lastIndexOf(".") + 1));
                    relator.setIdentifier(value);
                    relatedIdentifiers.add(relator);
                }

            }
        }
        schematicsMap.put("related_identifiers", relatedIdentifiers);
        schematicsMap.put("communities", communities);
        schematicsMap.put("contributors", contributors);
        schematicsMap.put("creators", creators);
        schematicsMap.put("grants", grants);
        schematicsMap.put("keywords", keywords);
        schematicsMap.put("references", references);

        String schematicsString = objectMapper.writeValueAsString(schematicsMap);
        objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        ZenodoDepositMetadata metadata = objectMapper.readValue(schematicsString, new TypeReference<ZenodoDepositMetadata>(){});

        deposit.setMetadata(metadata);
        deposit.getMetadata().setTitle(dmp.getLabel());
        deposit.getMetadata().setUploadType("publication");
        deposit.getMetadata().setPublicationType("datamanagementplan");
        deposit.getMetadata().setDescription((dmp.getDescription() != null && !dmp.getDescription().isEmpty() ? dmp.getDescription() : "<p></p>"));
        deposit.getMetadata().setVersion(String.valueOf(dmp.getVersion()));
        String zenodoCommunity = zenodoConfig.getCommunity();
        if(zenodoCommunity != null && !zenodoCommunity.isEmpty()) {
            ZenodoComunity community = new ZenodoComunity();
            community.setIdentifier(zenodoCommunity);
            deposit.getMetadata().getCommunities().add(community);
        }
        if (extraProperties.get("visible") == null) {
            deposit.getMetadata().setAccessRight(ZenodoAccessRight.RESTRICTED);
            deposit.getMetadata().setAccessConditions("");
        } else {
            if (((Boolean) extraProperties.get("visible"))) {
                Instant publicationDate = Instant.parse(extraProperties.get("publicDate").toString());
                if (publicationDate.isBefore(Instant.now())) {
                    deposit.getMetadata().setAccessRight(ZenodoAccessRight.OPEN);
                } else {
                    deposit.getMetadata().setAccessRight(ZenodoAccessRight.EMBARGOED);
                    deposit.getMetadata().setEmbargoDate(publicationDate.toString());
                }

                if (extraProperties.get("license") != null) {
                    deposit.getMetadata().setLicense(((Map<?, ?>) extraProperties.get("license")).get("pid").toString());
                }
            } else {
                deposit.getMetadata().setAccessRight(ZenodoAccessRight.RESTRICTED);
                deposit.getMetadata().setAccessConditions("");
            }
        }
        if (dmp.isPublic()) {
            ZenodoRelator relator = new ZenodoRelator();
            relator.setIdentifier(zenodoConfig.getDomain() + "/external/zenodo/" + dmp.getId().toString());
            relator.setRelation("isIdenticalTo");
            deposit.getMetadata().getRelatedIdentifiers().add(relator);
        }
        String zenodoAffiliation = zenodoConfig.getAffiliation();
        List<ZenodoContributor> contributors1 = dmp.getUsers().stream().map(userDMP -> {
            ZenodoContributor contributor = new ZenodoContributor();
            contributor.setName(userDMP.getUser().getName());
            contributor.setType("ProjectMember");
            if (dmp.getOrganisations() != null && !dmp.getOrganisations().isEmpty()) {
                contributor.setAffiliation(dmp.getOrganisations()
                        .stream().map(OrganisationDepositModel::getLabel).collect(Collectors.joining(", ")));
            } else {
                if(zenodoAffiliation != null && !zenodoAffiliation.isEmpty()) {
                    contributor.setAffiliation(zenodoAffiliation);
                }
            }
            return contributor;
        }).collect(Collectors.toList());

        List<ZenodoContributor> researchers = dmp.getResearchers().stream().map(researcher -> {
            ZenodoContributor contributor = new ZenodoContributor();
            contributor.setName(researcher.getLabel());
            contributor.setType("Researcher");
            String referenceHead = researcher.getReference().split(":")[0];
            String referenceTail = researcher.getReference().replace(referenceHead + ":", "");
            contributor.setAffiliation(referenceHead);
            if (referenceHead.equalsIgnoreCase("ORCID")) {
                contributor.setOrcid(referenceTail);
            }
            return contributor;
        }).collect(Collectors.toList());

        deposit.getMetadata().getContributors().addAll(contributors1);
        deposit.getMetadata().getContributors().addAll(researchers);

        if (dmp.getGrant() != null) {
            if (dmp.getGrant().getReference() == null) {
                dmp.getGrant().setReference("dmp:" + dmp.getGrant().getId());
            }
            String grantReferenceHead = dmp.getGrant().getReference().split(":")[0];
            if (grantReferenceHead.equals("openaire")) {
                String grantReferenceTail = dmp.getGrant().getReference().split(":")[3];
                List<DOIFunder> doiFunders = new ArrayList<>();
                try {
                    List<Map<String, Object>> tempdoiFunders = objectMapper.readValue(configLoader.getStreamFromPath(zenodoConfig.getDoiFunder()), List.class);
                    doiFunders = tempdoiFunders.stream().map(map -> objectMapper.convertValue(map, DOIFunder.class)).collect(Collectors.toList());
                } catch (IOException e) {
                    logger.error(e.getLocalizedMessage(), e);
                }
                if (dmp.getGrant().getFunder() != null && dmp.getGrant().getFunder().getLabel() != null) {
                    DOIFunder doiFunder = doiFunders.stream()
                            .filter(doiFunder1 -> dmp.getGrant().getFunder().getLabel().contains(doiFunder1.getFunder()) || doiFunder1.getFunder().contains(dmp.getGrant().getFunder().getLabel()))
                            .findFirst().orElse(null);
                    if (doiFunder != null) {
                        String finalId = doiFunder.getDOI() + "::" + grantReferenceTail;
                        ZenodoGrant grant = new ZenodoGrant();
                        grant.setId(finalId);
                        deposit.getMetadata().getGrants().add(grant);
                    }
                }
            }
        }
        ZenodoCreator creator = new ZenodoCreator();
        creator.setName(dmp.getUsers().stream().filter(userDMP -> userDMP.getRole().equals(UserDMPDepositModel.UserDMPRoles.OWNER.getValue())).findFirst().get().getUser().getName());
        if (dmp.getOrganisations() != null && !dmp.getOrganisations().isEmpty()) {
            creator.setAffiliation(dmp.getOrganisations()
                    .stream().map(OrganisationDepositModel::getLabel).collect(Collectors.joining(", ")));
        } else {
            if(zenodoAffiliation != null && !zenodoAffiliation.isEmpty()) {
                creator.setAffiliation(zenodoAffiliation);
            }
        }
        deposit.getMetadata().getCreators().add(creator);

        return deposit;
    }
}

