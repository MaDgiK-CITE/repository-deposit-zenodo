package eu.eudat.depositinterface.zenodorepository.models;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ZenodoAccessRight {
    RESTRICTED("restricted"), EMBARGOED("embargoed"), OPEN("open");

    private final String value;

    ZenodoAccessRight(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}
